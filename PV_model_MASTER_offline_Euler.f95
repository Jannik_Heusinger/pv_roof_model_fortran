
! PV model based on Jones and Underwood (2001)

! Modules needed later on
module my_functions

implicit none

contains
! Calculate cross product
FUNCTION cross(aa, bb)
    implicit none
    real, DIMENSION(3) :: cross
    real, DIMENSION(3), INTENT(IN) :: aa, bb

    cross(1) = aa(2) * bb(3) - aa(3) * bb(2)
    cross(2) = aa(3) * bb(1) - aa(1) * bb(3)
    cross(3) = aa(1) * bb(2) - aa(2) * bb(1)
END FUNCTION cross

end module my_functions

program PVmodel

use my_functions


IMPLICIT NONE



! the NOAH LSM has one ground surface temperature calculation which is modified by the vegetation fractions
! we might modify solar radiation in noahlsm and run it twice: for sunlit and shaded ground surface, and then average both according to the shaded fraction.
! The ground surface could then still be covered by vegetation

! import modules, files and all constants needed

!! Input variables and parameters
! global_rad: global radiation (W/m^2)
! doy: day of year (hourly)
! TA: ambient air temperature (K)
! u: wind velocity (m/s)
! T_ground: ground surface temperature (K)
! TZ: time zone relative to UTC
! XLAT: Latitude (in decimal degrees)
! Lon: Longitude (in decimal degrees)
! absorptivity: absorptivity of the PV module (dependent on the PV cell properties; may not be constant)
! em_roof: emissivity of the roof surface
! em_module: emissivity of the PV module
! DELT: time step (s)

! If boundary layer scheme for sensible heat calc. is used:
! dist: distance from the center of the PV module to the roof edge (usually varying with wind dir.)

! Initial conditions needed:
! T_module: temperature of the PV module (assumed to be uniform)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Declare parameters and variables  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! PV characteristics

real, PARAMETER :: alpha = 0.94            	! absorptivity of the cell surface
real, PARAMETER :: absorptivity = 1     	! this could be changed for semi-transparent/transparent PV panels
real, PARAMETER :: PV_albedo_min = 0.05     ! reflectivity of the PV surface
real, PARAMETER :: n = 1.398               	! index of the refraction of the cover glass
real, PARAMETER :: K_glazing = 4           	! glazing extinction coefficient (m-1)
real, PARAMETER :: em_module_up = 0.79     	! PV module upper surface emissivity
real, PARAMETER :: em_module_down = 0.95   	! PV module lower surface emissivity
real, PARAMETER :: h = 1.33            		! PV height above ground (of horizontal axis, m)
real, PARAMETER :: L = 1.956           		! length of PV module perpendicular to tilt axis (m)
real, PARAMETER :: W = 0.941            	! width of PV module, necessary for calculating Pout and 2 axes shade fraction calc
real, PARAMETER :: glass_thickness = 0.006  	! thickness of glazing layer (m)

! Location
real, PARAMETER :: XLAT = 52.27                ! Latitude (°)

! tracking system related constants
real, PARAMETER :: beta_max = 60.         ! maximum tilt angle of PV (horixontal 1-axis tracking system)
real, PARAMETER :: beta_night = 30.       ! tilt angle of PV at night (horixontal 1-axis tracking system)
real, PARAMETER :: beta_tilt = 10.        ! tilt angle for non-tracking and vertical axis tracking
real, PARAMETER :: gamma_h_1 = -15.       ! azimuth angle of fixed, tilted system (0 facing equator, west 90, east -90)
real, PARAMETER :: beta_dash = 15.        ! tilt angle of sloped axis (sloped 1-axis tracking system)
real, PARAMETER :: gamma_sloped = 0.      ! azimuth angle of sloped axis (0 facing equator, west 90, east -90)

! ground characteristics
real, PARAMETER :: em_roof = 0.7          ! Emissivity of the roof surface
real, PARAMETER :: roof_albedo = 0.3      ! Albedo of roof surface

! Location and time zone
!TZ = 7              ! MST relative to UTC

! Universal constants
real, PARAMETER :: sigma = 5.669*10.0**(-8.0)  ! Stefan-Boltzmann constant
real, PARAMETER :: Rv = 461.0                  ! Gas constant for water vapour (J K‐1 kg-1)

! PV power output
real, PARAMETER :: Eff_PV = 0.065           ! conversion efficiency of the PV module
real, PARAMETER :: CFF = 1.22               ! fill factor model constant (K m^2)
real, PARAMETER :: k1 = 10**6.0             ! constant=K/I0 (m^2 K)

! PV array
real, PARAMETER :: width_rows = 5.64   ! distance between rows (measured from one axis to the other, m)
real, PARAMETER :: length_rows = 200   ! length of PV panel rows in array (m)

! Bird solar model parameters
real, PARAMETER :: Ozone = 0.3
real, PARAMETER :: H2O = 1.5
real, PARAMETER :: Taua = 0.08
real, PARAMETER :: Ba = 0.85

! Sensible heat calc. parameters
! Boundary layer scheme
real, PARAMETER :: v_kin = 17.7*10.0**(-6.0) ! kinematic viscosity of air (m2/s)
real, PARAMETER :: kf = 0.025                ! thermal conductivity of air (W /(m K))
real, PARAMETER :: Pr = 0.71                 ! Prandtl number for air

! TARP scheme
real, PARAMETER :: Rf = 3.00                 ! Roughness parameter


real :: T_ground
real :: PI=4.0*ATAN(1.0)

!!!!!!!!!!!!!!!
!   Switches  !
!!!!!!!!!!!!!!!
integer, PARAMETER :: tracking_type = 2 ! 1=flat, 2=tilted, fixed, 3= horizontal tracking, 4=sloped axis tracking, 5= vertical axis tracking, 6= 2-axis tracking
integer, PARAMETER :: roof_type = 0     ! 0 = tilted or flat roof, PV panels parallel to roof (other roof types to be added later)
integer, PARAMETER :: PV_type = 2 	    ! 0 = Monocrystalline, 1 = Polychrystalline, 2 = Thinfilm
integer, PARAMETER :: Insulated = 0 	! 1 = insulated, 0 = not insulated
integer, PARAMETER :: Power_mod = 1 	! 1 = Masson, don't change in Fortran version
integer, PARAMETER :: QH_mod = 0 	    ! sensible heat calc. method; 0 = boundary layer scheme for individual panels (needs dist as input), 1 = TARP model for roof-averaged fluxes


integer, PARAMETER :: A = 1 ! surface area
real :: cell, glass, backing, insulation
real :: Cmodule
real :: beta_PV, gamma_h

real :: THEATAZ
real :: THEATAS    ! = PI/2. - THETAZ
real :: cos_theta_zh, theta_zh, gamma_sh

real :: cos_theta_h_dash, gamma_zero, sigma_gamma_one, sigma_gamma_two

real, dimension(3) :: P1, P2, P3, P4
real, dimension(3) :: P11, P21, P31, P41
real, dimension(3,3) :: R_z, R_y, R_x
real, dimension(3,4) :: point_matrix, rotated_matrix
real, dimension(3) :: p111,p222,p333,ps1, ps2, ps3, ps4
real, dimension(3) :: s,b,c,nn, d_interm
real :: dot_A, dot_B, c_interm
real :: shade_area, ground_area, shade_frc
real :: theta_h_degree, theta_h_rad, theta_r, tau_b, tau_nil
real :: IAM_b, theta_h_dif_degree, theta_h_dif_rad
real :: tau_dif, IAM_dif_sky, theta_h_difg_degree, theta_h_difg_rad
real :: tau_difg, IAM_difg

real :: theta_zh_degree, AM, M
real :: total_rad

real :: A_EW, B_EW, AAA, BBB, S1, S2, U, U_EW, S_EW, psi
real :: VF_angle1, VF_angle2, VF_angle
real :: width_rows_psi, phi, VF_angle_psi1, VF_angle_psi2
real :: VFangle_psi, PVF_PV, SVF_PV, GVF_PV, PAF
real :: alpha1, alpha_rad, beta, beta_0, beta_rad, beta_zero_dash
real :: cos_gamma_sh, cos_solar_alt, cos_theta_h, DELT
real :: direct_normal_rad, gamma_1, gamma_rad, hc_j, hc_kum
real :: hc_sharp, hc_test, l_char_kum, l_char_sharp
real :: l_char,l_char_test,r_perp_unpol,r_par_unpol,PV_albedo
real :: l_down, l_roof, l_pv_down, l_pv_in, l_pv_up, l_ratio
real :: l_shd, p_out, qconv, qconv_1
real :: qconv_2, qconv_3, qconv_4, qlw, sigma_beta, sigma_beta_dash
real :: sin_gamma_sh,gamma_sh1, sin_solar_alt, total_rad_clear_theoretical
real :: t_change, t_module, theta_h, ua,total_rad_temp
real :: SSG,SSGQ,OMG,DECLIN,COSZ,LLG,TA,GH,Kdif_model
real :: ETR, Air_mass, T_rayleigh, T_ozone, T_gases, T_water, T_aerosol, TAA
real :: rs, ld, las, ldnh, doy, press, therm_exp, dist
real :: Rey_up, Rey_down, Nu_up, Nu_down, h_Lien_up, h_Lien_down
real :: hc, hf1, hf2
complex :: gamma_sh_comp
logical :: file_exists

integer :: starttime = 0 ! generally 0
real(kind=8) :: deltatfrc ! timestep in hours
integer :: numfrc, timefrc_index, k
real :: counter
real(kind=8) :: timeis, timeend
real :: inputdt ! NEEDED?
real :: outpt_tm !
logical :: first_write
real :: Tnew,Told,Fold,Fold_prime,T_module2,qlw_ext

! Declare input variable arrays
real,allocatable,dimension(:) :: SSGfrc ! shortwave radiation (W m-2)
real,allocatable,dimension(:) :: SSGQfrc ! diffuse shortwave radiation (W m-2)
real,allocatable,dimension(:) :: OMGfrc ! solar hour angle (°)
real,allocatable,dimension(:) :: DECLINfrc ! Declination angle (°)
real,allocatable,dimension(:) :: COSZfrc ! cosine of solar zenith angle
real,allocatable,dimension(:) :: LLGfrc ! longwave downwelling radiation from sky (W m-2)
real,allocatable,dimension(:) :: TAfrc ! ambient air temperature (K)
real,allocatable,dimension(:) :: GHfrc ! modeled cloud free global radiation from Bird's model (W m-2)
real,allocatable,dimension(:) :: Kdif_modelfrc ! diffuse radiation from Bird model
real,allocatable,dimension(:) :: UAfrc ! wind speed (m/s)
real,allocatable,dimension(:) :: Tgroundfrc ! soil surface temperature (K)
real,allocatable,dimension(:) :: press_frc !
real,allocatable,dimension(:) :: dist_frc !
real,allocatable,dimension(:) :: doy_frc ! day of year

real(kind=8),allocatable,dimension(:) :: timefrc

! PV module material characteristics
if (PV_type == 1) then
    cell = A*0.00038*2330*712 ! Thickness (m), density (kg/m3), specific heat capacity (J/(kg*K))
    glass = A*glass_thickness*2500*840
    backing = A*0.00017*1475*1130
    insulation = A*0.1016*55*1210
else if (PV_type == 0) then
    cell = A*0.00086*2330*712
    glass = A*glass_thickness*2500*840
    backing = A*0.00017*1475*1130
    insulation = A*0.1016*55*1210
else
    cell = A*0.000001*2330*712
    glass = A*0.000051*1750*1050
    backing = A*0.000125*7900*477
    insulation = A*0.1016*55*1210
end if

! Heat capacity of PV module
if (Insulated == 1) then
    Cmodule = cell+backing+glass+insulation
else
    Cmodule = cell+backing+glass
end if

! Characteristic length for sensible heat flux calculation
L_char = 4*L*W/(2*L+2*W)

DELT = 30  ! Model time step (s)
outpt_tm = 1800.0 ! Output time step (s)

! Open OUTPUT files:

open(unit=837,file='W:/Heusinger/Modelle/pv_roof_model_fortran/PV.out',status='unknown', &
                       form='formatted')

write(837,845)'Tmodule','T_module2 ','Total rad ', 'SSG ', 'SSGQ ', &
'qconv ', 'P_out'
845 format (11(',', A))

! ATMOSPHERIC FORCING
INQUIRE(FILE="PV_validation_input.csv", EXIST=file_exists)

write(6,*) file_exists

!  open input atmospheric data file
! full path to file might be needed
open (981,file='W:/Heusinger/Modelle/pv_roof_model_fortran/PV_validation_input.csv')

read(981,*)numfrc,starttime,deltatfrc

!write(6,*)'numfrc = ',numfrc

allocate(SSGfrc(1:numfrc+1))
allocate(SSGQfrc(1:numfrc+1))
allocate(OMGfrc(1:numfrc+1))
allocate(DECLINfrc(1:numfrc+1))
allocate(COSZfrc(1:numfrc+1))
allocate(LLGfrc(1:numfrc+1))
allocate(TAfrc(1:numfrc+1))
allocate(UAfrc(1:numfrc+1))
allocate(Tgroundfrc(1:numfrc+1))
allocate(doy_frc(1:numfrc+1))
allocate(press_frc(1:numfrc+1))
allocate(dist_frc(1:numfrc+1))
allocate(timefrc(1:numfrc+1))

do k=1,numfrc
read(981,*) SSGfrc(k),SSGQfrc(k),OMGfrc(k),DECLINfrc(k),COSZfrc(k),&
            &LLGfrc(k),TAfrc(k), UAfrc(k), Tgroundfrc(k),&
            doy_frc(k), press_frc(k), dist_frc(k)

timefrc(k)=starttime+real(k-1)*deltatfrc

!write(6,*)'press_frc = ',press_frc(k)

enddo
close(981)



! initialize PV module temperature
T_module = TA
T_module2 = TA
! Initial values:
timeis=starttime ! current number of hours
timeend=starttime+deltatfrc*real(numfrc) ! total number of hours
timefrc_index=2

counter = 1.0
do while (timeis.le.timeend)

   !write(6,*)'TIMEIS = ',timeis

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! INTERPOLATE FORCING DATA  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   !write(6,*)'deltatfrc',deltatfrc

   if (timefrc(timefrc_index).le.timeis)  &
                   timefrc_index=min(numfrc+1,timefrc_index+1)

    SSG=SSGfrc(timefrc_index-1)+(timeis  &
                        -timefrc(timefrc_index-1))  &
          /deltatfrc*(SSGfrc(timefrc_index)-SSGfrc(timefrc_index-1))

    SSGQ=SSGQfrc(timefrc_index-1)+(timeis-timefrc(timefrc_index-1))  &
          /deltatfrc*(SSGQfrc(timefrc_index)-SSGQfrc(timefrc_index-1))
    OMG=OMGfrc(timefrc_index-1)+(timeis-timefrc(timefrc_index-1))  &
          /deltatfrc*(OMGfrc(timefrc_index)-OMGfrc(timefrc_index-1))
    TA= Tafrc(timefrc_index-1)+(timeis-timefrc(timefrc_index-1))  &
          /deltatfrc*(Tafrc(timefrc_index)-Tafrc(timefrc_index-1))
    DECLIN=DECLINfrc(timefrc_index-1)+(timeis-timefrc(timefrc_index-1))  &
          /deltatfrc*(DECLINfrc(timefrc_index)-DECLINfrc(timefrc_index-1))
! UA [m/s]   : Wind speed at 1st atmospheric level
    COSZ=COSZfrc(timefrc_index-1)+(timeis-timefrc(timefrc_index-1))  &
          /deltatfrc*(COSZfrc(timefrc_index)-COSZfrc(timefrc_index-1))
    LLG=LLGfrc(timefrc_index-1)+(timeis-timefrc(timefrc_index-1))  &
          /deltatfrc*(LLGfrc(timefrc_index)  &
                        -LLGfrc(timefrc_index-1))
    UA=UAfrc(timefrc_index-1)+(timeis  &
                        -timefrc(timefrc_index-1))  &
          /deltatfrc*(UAfrc(timefrc_index)  &
                        -UAfrc(timefrc_index-1))
    T_ground=Tgroundfrc(timefrc_index-1)+(timeis  &
                        -timefrc(timefrc_index-1))  &
          /deltatfrc*(Tgroundfrc(timefrc_index)  &
                        -Tgroundfrc(timefrc_index-1))
    doy=doy_frc(timefrc_index-1)+(timeis  &
                        -timefrc(timefrc_index-1))  &
          /deltatfrc*(doy_frc(timefrc_index)  &
                        -doy_frc(timefrc_index-1))
    press =  press_frc(timefrc_index-1)+(timeis  &
                        -timefrc(timefrc_index-1))  &
          /deltatfrc*( press_frc(timefrc_index)  &
                        - press_frc(timefrc_index-1))
    dist =  dist_frc(timefrc_index-1)+(timeis  &
                        -timefrc(timefrc_index-1))  &
          /deltatfrc*( dist_frc(timefrc_index)  &
                        - dist_frc(timefrc_index-1))

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Read and interpolate meteorological input to timestep
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!! Following input needed:
! global radiation (global_rad) --> SSG
! diffuse radiation (Kdif) --> SSGQ
! direct radiation (Kdir) --> SSGD
! solar hour angle (OMG) --> OMG
! solar altitude (THEATAS) --> THEATAS (rad)
! Solar zenith angle (theta_zh, deg) --> THEATAZ (rad)
! declination angle (DECLIN) --> DECLIN
! Longwave radiation from sky (W/m2, LLG_meas) --> LLG
! Air temperature at PV height (K, TA)   --> TA (K)
! relative humidity at PV height (%, RH)
! Ground surface temperature (K, T_ground)
! wind speed at PV height (m/s, u)  --> UA (m/s)
! Atmospheric pressure at PV height (mb, P) --> SFCPRS
! Latitude (deg, XLAT) --> XXLAT
! ground albedo(ground_albedo) --> ALBBRD (noahlsm)
! dist - distance from panel to roof edge - only needed for boundary layer QH calc., not for TARP QH calc.
! Longitude
! time step (dt) --> DELT
! day of year (doy)

THEATAS = ABS(ASIN(COSZ)) ! COSZ input from WRF
THEATAZ = ABS(ACOS(COSZ))

!if Power_mod == 'Sandia':
!    AA,NcellSer, Isc0, Imp0, Vmp0, aIsc, aImp, C0, C1, BVmp, DiodeFactor, C2, C3, a0, a1, a2, a3, a4, b0, b1, b2, b3, b4, b5, DELT0, fd, a, b = sandia_parameter_selection()

if (SSG < 0.) then
    SSG = 0.
end if

!! cosine of the zenit angle
cos_theta_zh = COSZ !cos(XLAT*PI/180)*cos(DECLIN*PI/180)*cos(OMG*PI/180)+sin(DECLIN*PI/180)*sin(XLAT*PI/180)

! zenit angle (degrees)
theta_zh = acos(cos_theta_zh)/PI*180
! hourly solar azimuth angle (degrees)
gamma_sh = asin((sin(OMG*PI/180)*cos(DECLIN*PI/180))/sin(theta_zh*PI/180))/PI*180

if (gamma_sh /= gamma_sh) then
    gamma_sh = -90.0
end if
!write(6,*)'gamma_sh_comp = ',gamma_sh_comp

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!   PV tracking types               !
! 1= fixed, horizontal              !
! 2= fixed, tilted                  !
! 3 = horizontal 1-axis tracking    !
! 4 = slope 1-axis tracking         !
! 5 = vertical 1-axis tracking      !
! 6 = 2-axes tracking               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (tracking_type == 1) then ! flat
        beta_PV = 0.
        gamma_h = 0. !azimuth of PV module
end if
if (tracking_type == 2) then ! tilted, fixed
        beta_PV = beta_tilt
        gamma_h = gamma_h_1
end if
if (tracking_type == 3) then
        ! for tracking system with horizontal axis, which is north-south oriented
        if (OMG < 0.) then
            gamma_h = -90.
        else
            gamma_h = 90.
        end if

        ! tilt angle (after Braun and Mitchell 1983)
        ! this is for a north-south oriented horizontal axis (panels tilt east-west)

        beta_0 = atan(tan(theta_zh*PI/180)*cos((gamma_h-gamma_sh)*PI/180))/PI*180

        if (beta_0 >= 0.) then
            sigma_beta = 0.
        else
            sigma_beta = 1.
        end if

        beta_PV = beta_0 + sigma_beta*180.

        if (beta_PV > beta_max) then
            beta_PV = beta_max
        end if
        if (cos_theta_zh<0.) then
            beta_PV = beta_night
        end if
end if

if (tracking_type == 4) then ! 1-axis sloped
      ! cos of incidence angle: angle between a direct solar ray and the surface normal
      cos_theta_h_dash = cos(theta_zh*PI/180)*cos(beta_dash*PI/180)+sin(theta_zh*PI/180)&
      &*sin(beta_dash*PI/180)*cos((gamma_sh-gamma_sloped)*PI/180)

      gamma_zero = gamma_sloped + atan((sin(theta_zh*PI/180)*sin((gamma_sh-gamma_sloped)&
      &*PI/180))/(cos_theta_h_dash*sin(beta_dash*PI/180)))/PI*180

      if ((gamma_zero-gamma_sloped)*(gamma_sh-gamma_sloped)>=0.) then
          sigma_gamma_one = 0.
      else
          sigma_gamma_one = 1.
      end if
      if ((gamma_sh-gamma_sloped)>=0) then
          sigma_gamma_two = 1.
      else
          sigma_gamma_two = -1.
      end if
      gamma_h = gamma_zero + sigma_gamma_one*sigma_gamma_two*180

      beta_zero_dash = atan(tan(beta_dash*PI/180)/cos(gamma_h*PI/180))/PI*180

      if (beta_zero_dash>=0.0) then
          sigma_beta_dash = 0.
      else
          sigma_beta_dash  = 1.
      end if
      beta_PV = beta_zero_dash + sigma_beta_dash*180
end if
    if (tracking_type == 5) then ! 1-axis vertical
        gamma_h = gamma_sh
        beta_PV = beta_tilt
    end if

    if (tracking_type == 6) then ! two-axis
        if (cos_theta_zh>0) then
            beta_PV = theta_zh
            gamma_h = gamma_sh
        else
            beta_PV = 30.
            gamma_h = 0.
        end if
    end if


! calculate cos of incidence angle: angle between a direct solar ray and the surface normal
cos_theta_h = cos(theta_zh*PI/180.0)*cos(beta_PV*PI/180.0)+sin(theta_zh*PI/180.0)*&
    sin(beta_PV*PI/180.0)*cos((gamma_sh-gamma_h)*PI/180.0)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Calculate global radiation for cloud free conditions after Bird 1984 !
! this is used as a safety measure at sunrise/sunset                   !
! for total radiation calc later on                                    !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

ETR =1367.0*(1.00011+0.034221*cos(2.0*PI*(doy-1.0)/365.0)+0.00128*sin(2.0*PI*(doy-1.0)/365.0)+0.000719*&
    cos(2.0*(2.0*PI*(doy-1.0)/365.0))+0.000077*sin(2.0*(2.0*PI*(doy-1.0)/365.0)))

if (theta_zh<89.0) then
        Air_mass = 1.0/(cos(theta_zh/(180.0/PI))+0.15/(93.885-theta_zh)**1.25)
    else
        Air_mass = 0.0
end if

if (Air_mass >0.0) then
    T_rayleigh = exp(-0.0903*(press*Air_mass/1013.0)**0.84*(1.0+press*Air_mass/1013.0-&
    (press*Air_mass/1013.0)**1.01))
    T_ozone = 1.0-0.1611*(Ozone*Air_mass)*(1.0+139.48*(Ozone*Air_mass))**-0.3034-0.002715*&
    (Ozone*Air_mass)/(1.0+0.044*(Ozone*Air_mass)+0.0003*(Ozone*Air_mass)**2.0)
    T_gases = exp(-0.0127*(Air_mass*press/1013.0)**0.26)
    T_water = 1.0-2.4959*Air_mass*H2O/((1.0+79.034*H2O*Air_mass)**0.6828+6.385*H2O*Air_mass)
    T_aerosol = exp(-(Taua**0.873)*(1.0+Taua-Taua**0.7088)*Air_mass**0.9108)
    TAA = 1.0-0.1*(1.0-Air_mass+Air_mass**1.06)*(1.0-T_aerosol)
    rs = 0.0685+(1.0-Ba)*(1.0-T_aerosol/TAA)
    ld = 0.9662*ETR*T_aerosol*T_water*T_gases*T_ozone*T_rayleigh
    las = ETR*cos(theta_zh/(180.0/PI))*0.79*T_ozone*T_gases*T_water*TAA*(0.5*(1.0-T_rayleigh)&
    +Ba*(1.0-(T_aerosol/TAA)))/(1.0-Air_mass+(Air_mass)**1.02)
end if

if (theta_zh < 90.0) then
    ldnh = ld*cos(theta_zh/(180.0/PI))
else
    ldnh = 0.0
end if

if (Air_mass > 0.0) then
     GH = (ldnh+las)/(1.0-roof_albedo*rs)
     Kdif_model = GH- ldnh
else
     GH = 0.0
     Kdif_model = 0.0
end if

! direct normal radiation
if (cos_theta_zh>0.0) then
    direct_normal_rad = (SSG-SSGQ)/cos_theta_zh
else
    direct_normal_rad = 0.0
end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! model of incident angle refraction losses (reflectivity of glazing in dependence on incident angle) !
! Calculate PV reflectivity based on incidence angle after De Soto et al. (2006)                      !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

theta_h_degree = acos(cos_theta_h)*180/PI

if (cos_theta_zh > 0.0) then
    ! for beam radiation
    theta_h_rad = acos(cos_theta_h)
    theta_r = asin(1/n*sin(theta_h_rad)) ! angle of refraction

    tau_b = exp(-((K_glazing*glass_thickness)/cos(theta_r)))*(1.0-1.0/2.0* &
        ((sin(theta_r-theta_h_rad)**2.0)/(sin(theta_r+theta_h_rad)**2.0)+tan(theta_r-theta_h_rad)**2.0 &
        /(tan(theta_r+theta_h_rad)**2.0)))
    tau_nil = exp(-K_glazing*glass_thickness)*(1-((1-n)/(1+n))**2) ! transmittance normal to sun
    IAM_b = tau_b/tau_nil ! IAM for beam radiation

    ! for diffuse radiation from sky
    theta_h_dif_degree = 59.68 - 0.1388*beta_PV + 0.001497*beta_PV**2
    theta_h_dif_rad = theta_h_dif_degree*PI/180.0
    theta_r = asin(1/n*sin(theta_h_dif_rad)) ! angle of refraction
    tau_dif = exp(-((K_glazing*glass_thickness)/cos(theta_r)))*(1.0-1.0/2.0*((sin(theta_r-theta_h_dif_rad)**2.0) &
        /(sin(theta_r+theta_h_dif_rad)**2.0)+tan(theta_r-theta_h_dif_rad)**2.0/ &
        (tan(theta_r+theta_h_dif_rad)**2.0)))
    tau_nil = exp(-K_glazing*glass_thickness)*(1-((1-n)/(1+n))**2) ! transmittance normal to sun
    IAM_dif_sky = tau_dif/tau_nil

    ! for diffuse radiation from ground
    theta_h_difg_degree = 90 - 0.5788*beta_PV + 0.002693*beta_PV**2
    theta_h_difg_rad = theta_h_difg_degree*PI/180.0
    theta_r = asin(1/n*sin(theta_h_difg_rad)) ! angle of refraction
    tau_difg = exp(-((K_glazing*glass_thickness)/cos(theta_r)))*(1.0-1.0/2.0*((sin(theta_r-theta_h_difg_rad)**2.0) &
        /(sin(theta_r+theta_h_difg_rad)**2.0)+tan(theta_r-theta_h_difg_rad)**2.0/ &
        (tan(theta_r+theta_h_difg_rad)**2.0)))
    tau_nil = exp(-K_glazing*glass_thickness)*(1-((1-n)/(1+n))**2) ! transmittance normal to sun
    IAM_difg = tau_difg/tau_nil

    ! reflectivity based on Fresnel's law
    theta_r = asin(1/n*sin(theta_h_rad)) ! angle of refraction (rad)
    if (theta_r < 0.00001) then
        theta_r = 0.00001
    end if
    r_perp_unpol = ((sin(theta_h_rad-theta_r))**2)/((sin(theta_r+theta_h_rad))**2)
    r_par_unpol = ((tan(theta_h_rad-theta_r))**2)/((tan(theta_r+theta_h_rad))**2)
    PV_albedo = (r_perp_unpol + r_par_unpol)/2

    if (tracking_type == 6) then
        PV_albedo = PV_albedo_min
    end if

else
    IAM_b = 1. ! IAM for beam radiation
    IAM_dif_sky = 1.
    IAM_difg = 1.
    PV_albedo = PV_albedo_min
end if

theta_zh_degree = acos(cos_theta_zh)*180/PI

! Air mass after King et al. 1998
AM = 1/(cos_theta_zh+0.5057*(96.080-theta_zh_degree)**(-1.634))

! Air mass modifier after DeSoto et al. 2006
M = 0.935823*AM**0.0+0.054289*AM**1.0+(-0.008677*AM**2.0)+0.000527*AM**3.0+(-0.000011*AM**4.0)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! total radiation on a tilted surface (reflectivity considered)   !
!           after Stackhouse 2016 p.26, Duffie and Beckman 1991   !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (cos_theta_zh>0.0) then
    if (SSGQ>SSG) then
        SSGQ = SSG-1.e-9
    end if
    total_rad_temp = (SSG-SSGQ)*(cos_theta_h/cos_theta_zh)*(1-PV_albedo)+ & ! contribution from direct solar rad.
    SSGQ*((1+cos(beta_PV*PI/180))/2)*(1-PV_albedo)+ &                       ! contribution from diffuse solar rad.
    SSG*roof_albedo*((1-cos(beta_PV*PI/180))/2)*(1-PV_albedo)             ! contribution from reflected rad. from ground

    total_rad = M*((SSG-SSGQ)*(cos_theta_h/cos_theta_zh)*tau_b*alpha+SSGQ* &
        ((1.0+cos(beta_PV*PI/180.0))/2.0)*tau_dif*alpha+SSG*roof_albedo*((1.0-cos(beta_PV*PI/180.0))&
        /2.0)*tau_difg*alpha)
    total_rad_clear_theoretical = (GH-Kdif_model)*(cos_theta_h/cos_theta_zh)+Kdif_model* &
        ((1.0+cos(beta_PV*PI/180.0))/2.0)+GH*roof_albedo*((1.0-cos(beta_PV*PI/180.0))/2.0)
    if (total_rad<0.0) then
        total_rad=0.0
        total_rad_temp=0.0
    end if
    if (total_rad > total_rad_clear_theoretical) then ! total radiation is not allowed to go above the theoretical total radiation for clear skies at the current time step
        total_rad = total_rad_clear_theoretical
        total_rad_temp = total_rad_clear_theoretical
    end if
else ! i.e. if the sun is below the horizon
    total_rad=SSGQ
    total_rad_temp=SSGQ
end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Calculate view factors for PV                                 !
! (assuming a row of PVs in front and behind), sky and ground   !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

psi = 90.0-abs(gamma_sh) ! 0 when east, 0 when west, 90 when south

L_shd = abs(L/(cos(psi*PI/180.0)))

if (abs(gamma_sh)<2) then
     L_shd = L
end if

A_EW = sin(beta_PV*PI/180.0)*L/2.0
B_EW = sin(beta_PV*PI/180.0)*L

AAA = sin(beta_PV*PI/180.0)*L_shd/2.0
BBB = sin(beta_PV*PI/180.0)*L_shd
S1 = (h-AAA+BBB)/tan(THEATAS*PI/180.0)
S2 = (h-AAA)/tan(THEATAS*PI/180.0)
U = L_shd * cos(beta_PV*PI/180.0)
U_EW = L * cos(beta_PV*PI/180.0)
S_EW = abs(S1 * cos(psi*PI/180.0))

VF_angle1 = tan(B_EW/2.0/(U_EW/2.0+width_rows))*180.0/PI
VF_angle2 = tan(B_EW/2.0/(width_rows-U_EW/2.0))*180.0/PI
VF_angle = VF_angle1 + VF_angle2

width_rows_psi = sqrt(width_rows**2.0+(length_rows/2.0)**2.0)
phi = tan((length_rows/2.0)/width_rows)  ! angle (rad)
VF_angle_psi1 = atan((B_EW/2.0)/(U_EW+width_rows_psi))*180.0/PI
VF_angle_psi2 = atan((B_EW/2.0)/(width_rows_psi-U_EW))*180.0/PI
VFangle_psi = VF_angle_psi1 + VF_angle_psi2

VF_angle = (VF_angle+VFangle_psi)/2.0

PVF_PV = VF_angle/180.0 ! PV view fraction of the PV
SVF_PV = (180.0-VF_angle)/180.0 ! sky view fraction of the PV
GVF_PV = (180.0-VF_angle)/180.0! ground view fraction of the PV

if (roof_type==0) then
    PVF_PV = 0
    SVF_PV = 1
    GVF_PV = 1
end if

! Calculate plan area fraction of PV needed later on
PAF = U/width_rows

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Longwave radiation transfer !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! weighted by the view fractions of the PV module, i.e. in a PV array each PV module partly "sees" the other modules in front and behind
! now implemented in "2.5D", later on we can think of implementing this in 3D

L_down = LLG * SVF_PV
L_PV_in = (PVF_PV/2.*sigma*em_module_up*T_module**4.+PVF_PV/2.*sigma*em_module_down*T_module**4.)!
L_roof = sigma*em_roof*T_ground**4.*GVF_PV !
L_PV_up = sigma*em_module_up*T_module**4.
L_PV_down = sigma*em_module_down*T_module**4.

! Longwave rad. heat exchange
qlw = L_down*em_module_up + L_roof*em_module_down - L_PV_up - L_PV_down +(1-em_roof)*L_PV_down*em_module_down
!ESK:
qlw_ext=L_down + L_roof+(1-em_roof)*L_down

!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Convective heat transfer !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!


if (QH_mod==0) then ! boundary layer scheme
    therm_exp = 1/TA
    Rey_up = UA * dist/v_kin  ! Reynolds number upper panel side
    Rey_down = 0.5 * UA * dist/v_kin ! lower panel side
    Nu_up = 0.0296 * Rey_up**0.8 * Pr**0.6 ! Nusselt number upper panel side
    Nu_down = 0.0296 * Rey_down**0.8 * Pr**0.6 ! lower panel side

    h_Lien_up = Nu_up * kf / dist   ! forced turbulent CHTC - upper panel side
    h_Lien_down = Nu_down * kf / dist ! forced turbulent CHTC - lower panel side

    qconv = (h_Lien_up+h_Lien_down) * (T_module - TA) ! Sensible heat flux - lower and upper panel side (W/m2)

else if (QH_mod==1) then ! TARP scheme
    hf1 = 2.537*((2*L+2*W)*UA/(L*W))**(0.5)         ! windward - used for upper side
    hf2 = 2.537*((2*L+2*W)*UA/(L*W))**(0.5)         ! windward - *1/2 added - used for lower side
    hc = hf1 + hf2
    qconv = hc * (T_module-TA)
end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Electrical power generation                                !
!                                                            !
! currently only Masson model implemented in Fortran version !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (Power_mod == 1) then
    ! After Masson et al. 2014
    P_out = total_rad*Eff_PV*min(1.0,1.0-0.005*(T_module-298.15))
else if (Power_mod == 2) then
    theta_h = acos(cos_theta_h)/PI*180
    ! Sandia model
    !P_out = SandiaModel(total_rad-SSGQ,SSGQ+ &
    ! ground_albedo * SSGQ*(sin(beta_PV*PI/180)), &
    ! UA,TA-273.15,theta_zh,T_module-273.15,theta_h, &
    ! AA,NcellSer, Isc0, Imp0, Vmp0, aIsc, aImp, C0, &
    ! C1, BVmp, DiodeFactor, C2, C3, a0, a1, a2, a3, &
    ! a4, b0, b1, b2, b3, b4, b5, DELT0, fd, a, b)

else if (Power_mod == 3) then
    !P_out = One_diode_Batzelis(total_rad, T_module)

else if (Power_mod == 4) then
    if (total_rad>0.0) then
      !Eff = Eff_PV*(1 - 0.00048*(T_module-25+275.15)+0.12*log(total_rad))
      !P_out = Eff * total_rad
    else
      P_out = 0.0
    end if
end if

!ESK:
!L_PV_in =
!(PVF_PV/2.*sigma*em_module_up*T_module**4.+PVF_PV/2.*sigma*em_module_down*T_module**4.)!
!L_PV_up = sigma*em_module_up*T_module**4.
!L_PV_down = sigma*em_module_down*T_module**4.
!L_PV_in = (PVF_PV/2.*sigma*em_module_up*T_module**4.+PVF_PV/2.*sigma*em_module_down*T_module**4.)!
!OLD:qlw = L_down + L_PV_in + L_ground+(1-em_ground)*L_down - L_PV_up - L_PV_down
!NEW:
!qlw_ext=L_down + L_ground+(1-em_ground)*L_down

!write(6,*) T_module,TA

! Newton method iteration to find the new module temperature (includes longwave
! from adjacent modules in temperature solution, but does not change PV_out
! through the iteration, even though it depends on T_module - too complicated)
Tnew=T_module2
Told=Tnew+999.
do 899 while (abs(Tnew-Told).gt.0.001)
  Told=Tnew
  Fold=-qlw_ext-total_rad_temp*absorptivity+P_out+hc_J*(Told-TA)- &
         -(PVF_PV/2.*sigma*em_module_up*Told**4.+ &
           PVF_PV/2.*sigma*em_module_down*Told**4.) &
         +sigma*em_module_up*Told**4.+sigma*em_module_down*Told**4.
  Fold_prime=hc_J-(4.*PVF_PV/2.*sigma*em_module_up*Told**3.+ &
                   4.*PVF_PV/2.*sigma*em_module_down*Told**3.) &
                  +4.*sigma*em_module_up*Told**3. &
                  +4.*sigma*em_module_down*Told**3.
  Tnew=-Fold/Fold_prime+Told
899 continue
! This is the new module temperature solved iteratively, assuming it has no heat capacity
  T_module2=Tnew



! Module temperature based on Euler time stepping (needs small DELT)
T_change = (qlw+absorptivity*total_rad_temp-P_out-qconv)/Cmodule  ! with absorption fraction calc after Duffie and Beckman 1991
T_module = T_module + DELT * T_change
!ESK:
  !write(6,*)'old,new Tmodule',T_module,T_module2,qlw

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Comparison with Masson model !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!    f_PV_masson = L/width_rows
!    T_module_masson = TA + 0.021*SSG
!    P_out_masson = SSG*Eff_PV*min(1,1-0.005*(T_module_masson-298.15))
!    LW_up_masson = em_module_up*sigma*T_module_masson**4+(1-em_module_up)*L_down
!    LW_down_PV_masson = sigma*TA**4
!    QH_masson = ((1-PV_albedo)*SSG+L_down-LW_up_masson+L_ground-LW_down_PV_masson)-P_out_masson


if (mod(counter,outpt_tm/DELT)==0) then
	first_write=.false.
write(837,844) T_module,T_module2, total_rad_temp, SSG, SSGQ, qconv,P_out
    write(6,*)'Counter = ',counter
    write(6,*)'timeis = ',timeis
    write(6,*)'T_change = ',T_change
    write(6,*)'T_module = ',T_module
    write(6,*)'dist = ',dist
    write(6,*)'qconv = ',qconv
    write(6,*)'T_ground = ',T_ground
end if

if (isnan(T_module)) stop '"T_module" is NaN'

timeis=timeis+DELT/3600.
counter = counter + 1.0
844  format(11(',',f12.3))

end do

end program PVmodel
